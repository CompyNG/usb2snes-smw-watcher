import websockets
import asyncio
import json
from typing import List
from enum import Enum
import watchgod
import sys
import pathlib
import os.path
import argparse


class Opcode(Enum):
    NAME = "Name"
    DEVICE_LIST = "DeviceList"
    ATTACH = "Attach"
    LIST = "List"
    MAKE_DIR = "MakeDir"
    PUT_FILE = "PutFile"
    BOOT = "Boot"


class ServerSpace(Enum):
    SNES = "SNES"
    FILE = "FILE"


class EnumJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return obj.value

        return super().default(obj)


class RequestType:
    opcode: Opcode = None
    space: ServerSpace = ServerSpace.SNES
    flags: str = None
    operands: List[str] = []

    def __init__(self, opcode: Opcode, space: ServerSpace, operands: List[str]):
        self.opcode = opcode
        self.space = space
        self.operands = operands

    def serialize(self):
        return EnumJsonEncoder().encode({
            "Opcode": self.opcode,
            "Space": self.space,
            "Flags": self.flags,
            "Operands": self.operands
        })


class ResponseType:
    results: List[str] = []

    def __init__(self, data):
        self.results = json.loads(data)['Results']


async def upload_rom_and_boot(ws: websockets.WebSocketClientProtocol, path: pathlib.Path):
    size = os.path.getsize(path)
    msg = RequestType(Opcode.PUT_FILE, ServerSpace.FILE, ["tmp/smw.sfc", f"{size:x}"])
    await ws.send(msg.serialize())

    print("Uploading...")
    with path.open('rb') as f:
        for chunk in iter(lambda: f.read(1024), b''):
            await ws.send(chunk)
    print("Done uploading")

    msg = RequestType(Opcode.BOOT, ServerSpace.FILE, ["tmp/smw.sfc"])
    await ws.send(msg.serialize())
    print("Booting")


async def watcher(path: pathlib.Path, args):
    print("Connecting")
    async with websockets.connect(f"ws://{args.server}:{args.port}/{args.path}") as ws:
        # Send opening "hello" message
        msg = RequestType(Opcode.NAME, ServerSpace.SNES, ["SmwWatcher"])
        await ws.send(msg.serialize())

        # Get list of available devices
        msg = RequestType(Opcode.DEVICE_LIST, ServerSpace.SNES, [])
        await ws.send(msg.serialize())
        recv = ResponseType(await ws.recv())
        connect_to = recv.results[0]
        if len(recv.results) > 1:
            print("More than one device, connecting to first anyway")

        # Attach to the device
        msg = RequestType(Opcode.ATTACH, ServerSpace.SNES, [connect_to])
        await ws.send(msg.serialize())

        # Check if tmp directory exists
        msg = RequestType(Opcode.LIST, ServerSpace.FILE, [""])
        await ws.send(msg.serialize())
        recv = ResponseType(await ws.recv())
        it = iter(recv.results)
        files = zip(it, it)
        found_tmp = False
        for f in files:
            if int(f[0]) == 0 and f[1] == "tmp":
                found_tmp = True
                break

        # Create it if not
        if not found_tmp:
            print("Couldn't find tmp")
            msg = RequestType(Opcode.MAKE_DIR, ServerSpace.FILE, ["tmp"])
            await ws.send(msg.serialize())

        await upload_rom_and_boot(ws, path)

        # Watch the ROM for changes
        async for changes in watchgod.awatch(path.parent):
            for change in changes:
                if change[0] == watchgod.Change.modified and path.samefile(change[1]):
                    await upload_rom_and_boot(ws, path)

        print("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("rom")
    parser.add_argument("--server", "-s", default="localhost")
    parser.add_argument("--port", "-p", type=int, default=8080)
    parser.add_argument("--path", default="")
    args = parser.parse_args()

    to_watch = pathlib.Path(args.rom)
    if not to_watch.exists():
        print(f"ERROR: Thing to watch doesn't exist ({to_watch})")
        sys.exit(1)

    if not to_watch.is_file():
        print(f"ERROR: Need a file to watch ({to_watch})")
        sys.exit(1)

    loop = asyncio.get_event_loop()
    fut = loop.create_task(watcher(to_watch, args))
    try:
        loop.run_until_complete(fut)
    except KeyboardInterrupt:
        print("Stopping...")
        fut.cancel()
        loop.run_until_complete(fut)
