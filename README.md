usb2snes SMW Watcher
====================

Intended for use with SMW and Lunar Magic, but could be useful for other games.

Watches a ROM file for changes, and does a push + boot to an sd2snes with
[usb2snes firmware](https://github.com/RedGuyyyy/sd2snes/releases/tag/usb2snes_v9) when detected.  Requires that the
usb2snes server is running.
